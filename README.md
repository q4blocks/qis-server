# deploy
```sh
gcloud config set project quality-tutor-engine
gradle appengineDeploy

# clean in case of generated build files in /build results in Eclipse error complaining
```

Ignore test when build:
```
gradle build -x test

# example continuous testing
gradle test --continuous
```