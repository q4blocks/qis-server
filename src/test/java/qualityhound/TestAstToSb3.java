package qualityhound;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import ast.AssignStmt;
import ast.BlockSeq;
import ast.BroadcastStmt;
import ast.ExprStmt;
import ast.IfElseStmt;
import ast.LoopStmt;
import ast.OperatorExpr;
import ast.ProcAccess;
import ast.ProcDecl;
import sb3.parser.xml.Sb3XmlParser;


public class TestAstToSb3 {
	Sb3XmlParser parser;
	@Before
	public void setup() {
		 parser = new Sb3XmlParser();
	}
	
	@Test
	public void test() {
		String xml1 = "<block type='operator_random' id='id5' x='23' y='236'><value name='FROM'><shadow type='math_number' id='YBq#f_Sz}_n3X#CxkD6}'><field name='NUM'>1</field></shadow><block type='operator_add' id='Bg$IFvPa3@xoSL0F[+5V'><value name='NUM1'><shadow type='math_number' id='U%}{r(|5egNx[H)CrcAM'><field name='NUM'>1</field></shadow></value><value name='NUM2'><shadow type='math_number' id=';9IW!mI%#IW@Z*(KObzp'><field name='NUM'>2</field></shadow></value></block></value><value name='TO'><shadow type='math_number' id='K`13_.4E^jD.ePtUCB=U'><field name='NUM'>10</field></shadow><block type='operator_subtract' id='AnLHdDt(8aD0_C[R2;;4'><value name='NUM1'><shadow type='math_number' id='7:lv4{Gggh7$kRVTuI$L'><field name='NUM'></field></shadow><block type='operator_multiply' id='9JqY#CG1#~c-)/ELR1qT'><value name='NUM1'><shadow type='math_number' id='mqC[O]{Kg78R6F9@34H5'><field name='NUM'>3</field></shadow></value><value name='NUM2'><shadow type='math_number' id='[-7NR4Kt5_.Y{ACsTqrf'><field name='NUM'>4</field></shadow></value></block></value><value name='NUM2'><shadow type='math_number' id='uhHI-FJF@Y5G?%K4o(FF'><field name='NUM'></field></shadow><block type='operator_divide' id='@tQ`DImqU8|B;@-w+H%O'><value name='NUM1'><shadow type='math_number' id='qQUUw1VRnPvllHvd|uVw'><field name='NUM'>5</field></shadow></value><value name='NUM2'><shadow type='math_number' id='fPRjl48I?nk@;z|{42?q'><field name='NUM'>6</field></shadow></value></block></value></block></value></block>";
		OperatorExpr node = (OperatorExpr) parser.parseBlock(xml1);
		String res = node.toSb3().replaceAll("\\s","");
		String expected = "pick random ((1)+(2)) to (((3) * (4))-((5)/(6)))";
		System.out.println(node.toSb3());
		
		assertThat(res,equalTo(expected.replaceAll("\\s","")));
	}
	@Test
	public void simpleBlockTest() {
		String xml = "<block type='motion_movesteps' id='*`#p/~R4kNb@uT|}iU@u' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='{H`(3cDLOp*BR%OM3t1j'><field name='NUM'>10</field></shadow></value></block>";
		ExprStmt stmt = (ExprStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testSimpleSequence() {
		String xml = "<block type='motion_movesteps' id='5Uw?3q^=yB5jraKNTL,}' x='69' y='235'><value name='STEPS'><shadow type='math_number' id='Qj:IJlDZ9(sm{Mg{y#M!'><field name='NUM'>10</field></shadow><block type='operator_random' id='c=|(=I*@Ypu_n#kc^Tlv'><value name='FROM'><shadow type='math_number' id='qn%aR|Z;RXONzuqmw-Q:'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='{,I{{}w.#C`F%)|E%n%@'><field name='NUM'>10</field></shadow></value></block></value><next><block type='motion_turnright' id='F~6fW6H4xG+,~:]=8!.x'><value name='DEGREES'><shadow type='math_number' id='FuvX123.ITWRO%nN`LmO'><field name='NUM'>15</field></shadow></value></block></next></block>";
		BlockSeq res = parser.parseBlockSeq(xml);
		System.out.println(res.toSb3());
	}
	
	@Test
	public void testLoop() {
		String xml = "<block type='control_repeat' id=')JGbc:rrCYH.N{2-lB-}' x='164' y='161'><value name='TIMES'><shadow type='math_whole_number' id='{B]R5f.+j!5aXB60Z0I.'><field name='NUM'>10</field></shadow><block type='operator_add' id='2U!q@GuUItnmjp4-oLKK'><value name='NUM1'><shadow type='math_number' id='FSZ}|r2epeb5-KjZ9ACu'><field name='NUM'>1</field></shadow></value><value name='NUM2'><shadow type='math_number' id='07=(HMSi!)XSbE`7BZbp'><field name='NUM'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='control_repeat' id='rgq0g5mB8Zv`H+EFFVEc'><value name='TIMES'><shadow type='math_whole_number' id='{?f=m#@#],*|,y/@TUAv'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='1+ns0A;C[_c2%=X]prH@'><value name='STEPS'><shadow type='math_number' id='_KH))[PLJ6{*0ne=o#jB'><field name='NUM'>10</field></shadow></value></block></statement></block></statement></block>";
		LoopStmt stmt = (LoopStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testLoop2() {
		String xml = "<block type='control_repeat_until' id='e%SD`W`6/3.4-;^=w#%l' x='155' y='132'><value name='CONDITION'><block type='operator_lt' id='gY)4~K^zDw,sXSc97Rf='><value name='OPERAND1'><shadow type='text' id='qUx$|R(lgRZWvMh,rdnH'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='JnLkgK|Tz}O!7gZgr$?b'><field name='TEXT'>4</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='Z023MbAb^?Mw7w=Flg8h'><value name='STEPS'><shadow type='math_number' id='_6G`K|=_a8h]NFm*kPkB'><field name='NUM'>10</field></shadow></value></block></statement></block>";
		LoopStmt stmt = (LoopStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	
	@Test
	public void testIfThen() {
		String xml = "<block type='control_if' id='HeZ_m*OB4cQjrnXqjkQr' x='120' y='203'><value name='CONDITION'><block type='operator_lt' id='ky21-@)yhy{nu`G8A*2o'><value name='OPERAND1'><shadow type='text' id='hL|anOH1yQB*rBs~ighB'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id=']BDwE:}Idm]n+b5}*U^i'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='`;b@BGC97n2e^FMF@E|{'><value name='STEPS'><shadow type='math_number' id='+%?H46iqeVz+=}70#!F1'><field name='NUM'>10</field></shadow></value></block></statement></block>";
		IfElseStmt stmt = (IfElseStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testIfThenElse() {
		String xml = "<block type='control_if_else' id='j|-E@VbJsULHI5(mHE:*' x='62' y='248'><value name='CONDITION'><block type='operator_lt' id='9XH|n%YC5!BXgW!+]VV9'><value name='OPERAND1'><shadow type='text' id='LL(wyv$kY_UTJ=S]Ps)%'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='pIn6BY5XCEgpi~JQ,y$`'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='D!GQT_;1n6^I[Irou~r('><value name='STEPS'><shadow type='math_number' id='a2sd,|1dhlfW;Rbi_rQ6'><field name='NUM'>10</field></shadow></value></block></statement><statement name='SUBSTACK2'><block type='motion_turnright' id='Q;Ep3WR4Rg1v26qyQS0Q'><value name='DEGREES'><shadow type='math_number' id='6?D8-C}G`?(VtT[lZ-,N'><field name='NUM'>15</field></shadow></value></block></statement></block>";
		IfElseStmt stmt = (IfElseStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testCustomBlock() {
		String xml = "<block type='procedures_definition' id=';@gt+uIE3qmkO1hXP#qC' x='176' y='130'><statement name='custom_block'><shadow type='procedures_prototype' id='CcvK%9U^sZnb;]I0kRq7'><mutation proccode='test %s' argumentids='[&quot;M9]ZYkEtx#K*9LbQUXg8&quot;]' argumentnames='[&quot;num&quot;]' argumentdefaults='[&quot;todo&quot;]' warp='false'></mutation><value name='M9]ZYkEtx#K*9LbQUXg8'><shadow type='argument_reporter_string_number' id=',_8[DXagHxPp~Pc`9GXk'><field name='VALUE'>num</field></shadow></value></shadow></statement><next><block type='motion_movesteps' id='iLGX{^2}];lo._rX-)$t'><value name='STEPS'><shadow type='math_number' id='RN^ek2kkLD5EWK]?VO%_'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id='E_pBB8LJ2O{xz#U+RP@+'><field name='VALUE'>num</field></block></value></block></next></block>";
		ProcDecl def = (ProcDecl) parser.parseProcDecl(xml);
		System.out.println(def.toSb3());
	}
	
	@Test
	public void testCustomBlockBooleanParam() {
		String xml = "<block type='procedures_definition' id=';oC~sM3$PpV{i0ERfs8,' x='145' y='283'><statement name='custom_block'><shadow type='procedures_prototype' id='zz[?/=KVt/Q!HR1E%a*8'><mutation proccode='block name %s %b' argumentids='[&quot;P)xRFz`.=Ro}aMOs^mHY&quot;,&quot;3%Aw,/nh/=x|t@HL)Tj4&quot;]' argumentnames='[&quot;number or text&quot;,&quot;boolean&quot;]' argumentdefaults='[&quot;todo&quot;,&quot;todo&quot;]' warp='false'></mutation><value name='P)xRFz`.=Ro}aMOs^mHY'><shadow type='argument_reporter_string_number' id='oW%AijKn^LUTPt)285wo'><field name='VALUE'>number or text</field></shadow></value><value name='3%Aw,/nh/=x|t@HL)Tj4'><shadow type='argument_reporter_boolean' id='z@.9iG$6ETy`#)/5oG/U'><field name='VALUE'>boolean</field></shadow></value></shadow></statement><next><block type='control_wait_until' id='eON[y?C7nq/uG-;hG;m2'><value name='CONDITION'><block type='argument_reporter_boolean' id='mIkmmTQUy%kl(~!rM=d{'><field name='VALUE'>boolean</field></block></value></block></next></block>";
		ProcDecl def = (ProcDecl) parser.parseProcDecl(xml);
		System.out.println(def.toSb3());
	}
	
	@Test
	public void testAll() {
		String xml = "<block type='procedures_definition' id=';oC~sM3$PpV{i0ERfs8,' x='145' y='283'><statement name='custom_block'><shadow type='procedures_prototype' id='zz[?/=KVt/Q!HR1E%a*8'><mutation proccode='block name %s %b' argumentids='[&quot;P)xRFz`.=Ro}aMOs^mHY&quot;,&quot;3%Aw,/nh/=x|t@HL)Tj4&quot;]' argumentnames='[&quot;number or text&quot;,&quot;boolean&quot;]' argumentdefaults='[&quot;todo&quot;,&quot;todo&quot;]' warp='false'></mutation><value name='P)xRFz`.=Ro}aMOs^mHY'><shadow type='argument_reporter_string_number' id='oW%AijKn^LUTPt)285wo'><field name='VALUE'>number or text</field></shadow></value><value name='3%Aw,/nh/=x|t@HL)Tj4'><shadow type='argument_reporter_boolean' id='z@.9iG$6ETy`#)/5oG/U'><field name='VALUE'>boolean</field></shadow></value></shadow></statement><next><block type='motion_movesteps' id='Q*q}/~2f`S7:Fj!:[D(J'><value name='STEPS'><shadow type='math_number' id='6F0;nxT]db!{8g%u-48,'><field name='NUM'>10</field></shadow></value><next><block type='control_if' id='LH!(i:LtwvZ3!6qJ@.;$'><value name='CONDITION'><block type='operator_and' id='+SZgb;:+aX-EWzU0xyGw'><value name='OPERAND1'><block type='operator_lt' id='V6e2_EbQEng%|:Ak:Evm'><value name='OPERAND1'><shadow type='text' id='4:Z52?V6;5T0-,h~/?oE'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='d5Xx$Hn(.?7*`7MQ%tB8'><field name='TEXT'>2</field></shadow></value></block></value><value name='OPERAND2'><block type='operator_equals' id='q%aqYqq,I0=#v|0[QPNq'><value name='OPERAND1'><shadow type='text' id='6kUM6o(nLVt|8WoH5(@Y'><field name='TEXT'></field></shadow></value><value name='OPERAND2'><shadow type='text' id='BO7W7OVNS9co+VIEvCA!'><field name='TEXT'></field></shadow></value></block></value></block></value><statement name='SUBSTACK'><block type='looks_hide' id='x3_YAq-]:9,FEV206}(q'><next><block type='control_wait_until' id='eON[y?C7nq/uG-;hG;m2'><value name='CONDITION'><block type='argument_reporter_boolean' id='mIkmmTQUy%kl(~!rM=d{'><field name='VALUE'>boolean</field></block></value></block></next></block></statement></block></next></block></next></block>";
		ProcDecl def = (ProcDecl) parser.parseProcDecl(xml);
		System.out.println(def.toSb3());
	}
	
	@Test
	public void testBlockDropdown() {
		String xml = "<block type='motion_goto' id='ci`LKKbtAXc]6`a-]4bD' x='99' y='178'><value name='TO'><shadow type='motion_goto_menu' id='gn(Yp`eyvb)}a]Q.vi_P'><field name='TO'>_mouse_</field></shadow></value><next><block type='motion_setrotationstyle' id='.u]CaVX3.?DEu37$3%?W'><field name='STYLE'>left-right</field><next><block type='motion_movesteps' id='/U0F:gIAL)a%a|rpo/y,'><value name='STEPS'><shadow type='math_number' id='^twqis9S|j[3Ly@04-=R'><field name='NUM'>10</field></shadow><block type='operator_mathop' id='Tl?O_sOdRJ]FY:(4ClQ5'><field name='OPERATOR'>abs</field><value name='NUM'><shadow type='math_number' id='aGQF:C+qG%%[lVA^PjY-'><field name='NUM'>12.4</field></shadow></value></block></value></block></next></block></next></block>";
		BlockSeq res = parser.parseBlockSeq(xml);
		System.out.println(res.toSb3());
	}
	
	@Test
	public void test3() {
		String template = "do %s for %s times";
		String generated = String.format(template,Lists.newArrayList("1","2").toArray());
		System.out.println(generated);
	}
	
	@Test
	public void testSb3forBlockInDropdownInput() {
		String xml = "<block type='looks_switchcostumeto' id='Jm8VE0:2JXG.]UxJmcqV' x='137' y='261'><value name='COSTUME'><shadow type='looks_costume' id='J!e`qYIN!s}|lg7E#3{*'><field name='COSTUME'>COSTUME1</field></shadow><block type='operator_letter_of' id='bXZm}{!J/HJ##Hxi@@-R'><value name='LETTER'><shadow type='math_whole_number' id='FYj0?;F=Lm0wD6]BUbvX'><field name='NUM'>1</field></shadow><block type='data_variable' id='aE]EeaT`V-L2;6z!}v5,'><field name='VARIABLE' id='T@0P_$vY)GM6;@e,:XV7' variabletype=''>b</field></block></value><value name='STRING'><shadow type='text' id='dspF-{_9CplM7HQgTd]p'><field name='TEXT'>world</field></shadow><block type='data_variable' id='FaJn.uvmY5;!P3JjN1uF'><field name='VARIABLE' id='^XzXOBe~?m+UyZ7Kw}(O' variabletype=''>a</field></block></value></block></value></block>";
		ExprStmt stmt = (ExprStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testProcAccess() {
		String xml = "<block type='procedures_call' id='id1' x='253' y='345'><mutation proccode='a' argumentids='[]' warp='null'></mutation></block>";
		ProcAccess callBlock = (ProcAccess) parser.parseBlock(xml);
		System.out.println(callBlock.toSb3());
	}
	
	@Test
	public void testProcAccess1Arg() {
		String xml = "<block type='procedures_call' id='*A!DfY+agW@!,cIa3LD0' x='142' y='255'><mutation proccode='a %n' argumentids='[&quot;input0&quot;]' warp='null'></mutation><value name='input0'><shadow type='math_number' id='hbb|VS|Uz4gg.gWtC6~J'><field name='NUM'>1</field></shadow></value><next><block type='motion_movesteps' id='JPC?:9Zx`/H.~-gzW@4r'><value name='STEPS'><shadow type='math_number' id='`:;.1ch}T9wfOOso9=hN'><field name='NUM'>10</field></shadow></value></block></next></block>";
		ProcAccess callBlock = (ProcAccess) parser.parseBlock(xml);
		System.out.println(callBlock.getMeta().getProcCode());
		System.out.println(callBlock.toSb3());
	}
	
	@Test
	public void testChangeVarBy() {
		String xml = "<block type='data_setvariableto' id='nF+qF_-mzNg/Eh89[d1w' x='251' y='197'><field name='VARIABLE' id='ko1FK)ur_}5EieM92:bT' variabletype=''>a</field><value name='VALUE'><shadow type='text' id='^_?qL7mN8_*S,iOzoL|X'><field name='TEXT'>0</field></shadow></value></block>";
		AssignStmt stmt = (AssignStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	
	@Test
	public void testBroadcast() {
		String xml = "<block type='event_broadcast' id='-7F=82Mo!a$`xkrc1zTT' x='200' y='319'><value name='BROADCAST_INPUT'><shadow type='event_broadcast_menu' id='Qs[aAlpMd}lX7M/@B^]c'><field name='BROADCAST_OPTION' id='1]]|SKIAGC(^sqk?dIwk' variabletype='broadcast_msg'>message1</field></shadow></value></block>";
		BroadcastStmt stmt = (BroadcastStmt) parser.parseBlock(xml);
		System.out.println(stmt.toSb3());
	}
	

	
	
}