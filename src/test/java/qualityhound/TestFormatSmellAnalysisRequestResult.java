package qualityhound;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import anlys.smell.DuplicateCode;
import anlys.smell.DuplicateCodeAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import qtutor.adapters.DuplicateCodeResultListAdapter;
import sb3.parser.xml.Sb3XmlParser;
import util.UtilFunc;

public class TestFormatSmellAnalysisRequestResult {
	DuplicateCodeAnalyzer analyzer;
	Program program;
	@Before
	public void setup() {
		analyzer = new DuplicateCodeAnalyzer();
		
	}
	
	@Test
	public void test() {
		String xml = "<program><stage name='Stage' x='0' y='0' size='100' direction='90' visible='true'><xml><costumes><costume name='backdrop1'/></costumes><sounds><sound name='pop'/></sounds><variables><variable type='' id='`jEk@4|i[#Fk?(8x)AV.-my variable' islocal='false' iscloud='false'>my variable</variable></variables></xml></stage><sprite name='Sprite1' x='0' y='0' size='100' direction='90' visible='true'><xml><costumes><costume name='costume1'/><costume name='costume2'/></costumes><sounds><sound name='Meow'/></sounds><variables></variables><block id='^RbOx_7T(S;bvsaa}x{B' type='event_whenflagclicked' x='101' y='22' ><next><block id='/o8Xc,Iw(Sd]Wg3H9glN' type='motion_movesteps' ><value name='STEPS'><shadow id=',91)P~oZd?hCJ?B.[,TU' type='math_number' ><field name='NUM'>10</field></shadow></value><next><block id='BI;V~uF#mvgq:`KC2cVv' type='motion_turnright' ><value name='DEGREES'><shadow id='dRi1),N~0@q-uj#I2Mn.' type='math_number' ><field name='NUM'>20</field></shadow></value><next><block id='q8eYqcXpwXu*4:/5FzM*' type='motion_turnleft' ><value name='DEGREES'><block id='EqSjY(=(2@:U{JOfWWl1' type='operator_add' ><value name='NUM1'><shadow id='^k90Ya.6l1:qgWb;3-O3' type='math_number' ><field name='NUM'>10</field></shadow></value><value name='NUM2'><shadow id='C1;,45m-).(QV5Zi1.]s' type='math_number' ><field name='NUM'>20</field></shadow></value></block><shadow id='RwJA=c}5pE6o#q,spcR:' type='math_number' ><field name='NUM'>30</field></shadow></value></block></next></block></next></block></next></block>,<block id='ij-oIeA;|1b5J*%yl]%2' type='event_whenflagclicked' x='421' y='52' ><next><block id='Muy2@Rx^5]ESJJqDJ1f_' type='motion_movesteps' ><value name='STEPS'><shadow id='a{:sGJ-l.5%ExCyo9@h9' type='math_number' ><field name='NUM'>10</field></shadow></value><next><block id='Wgv.jsTmUR`zrkf9A$p[' type='motion_turnright' ><value name='DEGREES'><shadow id='TJ~qhLtJ`6X9BA-J;4Yv' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='x.hB}M_+_kz*?f7Jp;s6' type='motion_turnleft' ><value name='DEGREES'><shadow id='z{G{Rp3VOLc{Dg,U%+K:' type='math_number' ><field name='NUM'>15</field></shadow></value></block></next></block></next></block></next></block></xml></sprite></program>";
		program = new Sb3XmlParser().parseProgram(xml);
		SmellListResult<DuplicateCode> smellRes = analyzer.analyze(program);
		String jsonRes = new DuplicateCodeResultListAdapter(smellRes).toJson();
		System.out.println(jsonRes);
	}
	
	@Test
	public void test2() throws IOException {
		File file = new File(UtilFunc.class.getClassLoader().getResource("test3.xml").getFile());
		String xml = FileUtils.readFileToString(file);
		program = new Sb3XmlParser().parseProgram(xml);
		SmellListResult<DuplicateCode> smellRes = analyzer.analyze(program);
		String jsonRes = new DuplicateCodeResultListAdapter(smellRes).toJson();
		System.out.println(jsonRes);
	}

}
