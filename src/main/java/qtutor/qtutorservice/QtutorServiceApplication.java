package qtutor.qtutorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ast.StrLiteral;

@SpringBootApplication
@RestController
public class QtutorServiceApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(QtutorServiceApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(QtutorServiceApplication.class, args);
	}

	@RequestMapping(value = "/")
	public String success() {
		StrLiteral lit = new StrLiteral("Quality4All Engine");
		return lit.toDevString()+", is running!";
	}
}
