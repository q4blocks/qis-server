package qtutor.qtutorservice;

import anlys.refact.RefactResult;
import anlys.smell.CodeSmell;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AnalysisRecord {
	private String id;
	private CodeSmell smell;
	private RefactResult refactoring;
}
