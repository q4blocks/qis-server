package qtutor.qtutorservice;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.hash.Hashing;

import anlys.refact.ExtractConst;
import anlys.refact.ExtractParentSprite;
import anlys.refact.ExtractProcedure;
import anlys.refact.ExtractProcedureReq;
import anlys.refact.RefactAnalyzer;
import anlys.refact.RefactRequest;
import anlys.refact.RefactResult;
import anlys.refact.RescopeVar;
import anlys.smell.BroadScopeVarAnalyzer;
import anlys.smell.CodeSmell;
import anlys.smell.DuplicateCode;
import anlys.smell.DuplicateCodeAnalyzer;
import anlys.smell.DuplicateConstAnalyzer;
import anlys.smell.DuplicateSpriteAnalyzer;
import anlys.smell.SmellAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import lombok.extern.slf4j.Slf4j;
import qtutor.adapters.DuplicateCodeResultListAdapter;
import qtutor.adapters.ExtractCustomBlockParams;
import sb3.parser.xml.Sb3XmlParser;

@Slf4j
@RestController
public class AnalysisController {
	Sb3XmlParser sb3Parser = new Sb3XmlParser();
	Map<String, String> id2XmlSha = Maps.newHashMap();
	Map<String, Map<String, Object>> cache = Maps.newHashMap();
	Program program = null;

	public String smellAnalyzeHelper(String xml) {
		DuplicateCodeAnalyzer analyzer = new DuplicateCodeAnalyzer();
		Program program = new Sb3XmlParser().parseProgram(xml);
		SmellListResult<DuplicateCode> smellRes = analyzer.analyze(program);
		String jsonRes = new DuplicateCodeResultListAdapter(smellRes).toJson();
		return jsonRes;
	}

	@CrossOrigin
	@PostMapping("/smell-analyze")
	public Map<String, Object> smellAnalyze(@RequestBody String xml, @RequestHeader("id") String projectId,
			@RequestHeader("type") String type) {
		String jsonStringResp = "";
		try {
			switch (type) {
			case "duplicate_code":
				jsonStringResp = smellAnalyzeHelper(xml);
				break;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return ImmutableMap.<String, Object>builder().put("id", projectId).put(type, jsonStringResp).build();
	}

	@CrossOrigin
	@PostMapping("/transform")
	public Map<String, Object> transform(@RequestBody String xml, 
			@RequestHeader("type") String type, @RequestHeader("params") String paramString) {
		Map<String, Object> resp = Maps.newHashMap();
		
		ObjectMapper mapper = new ObjectMapper();
		ExtractCustomBlockParams params = null;
		
		try {
			log.info(paramString);
			program = sb3Parser.parseProgram(xml);
			params = mapper.readValue(paramString, ExtractCustomBlockParams.class);
			ExtractProcedureReq req = ExtractProcedureReq.from(params.getTarget(), params.getStmtIdFragments(), program);
			RefactResult result = new ExtractProcedure().analyze(program.deepCopy(), req);
//			log.info(result.toPrettyJson());
			resp.put("result", result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return resp;
	}

	@CrossOrigin
	@PostMapping("/analyze")
	public Map<String, Object> analyze(@RequestBody String xml, @RequestHeader("id") String projectId,
			@RequestHeader("type") String type) {
		String sha256hex = Hashing.sha256().hashString(xml, StandardCharsets.UTF_8).toString();
		// if (id2XmlSha.containsKey(projectId) &&
		// id2XmlSha.get(projectId).equals(sha256hex)) {
		// log.info("found result for {} in cache", projectId);
		// return cache.get(projectId);
		// }

		log.info("Received analysis request for project id:{}, type:{}", projectId, type);
		log.trace(xml);

		try {
			program = sb3Parser.parseProgram(xml);
		} catch (Exception e) {
			log.error("error parsing program xml {}", e);
			return ImmutableMap.<String, Object>builder().put("id", projectId).put("type", type)
					.put("error", e.getMessage()).build();
		}

		Map<String, Object> resp = Maps.newHashMap();
		try {
			switch (type) {
			case "duplicate_code":
				resp = process(new DuplicateCodeAnalyzer(), new ExtractProcedure());
				break;
			case "duplicate_sprite":
				resp = process(new DuplicateSpriteAnalyzer(), new ExtractParentSprite());
				break;
			case "duplicate_constant":
				DuplicateConstAnalyzer dcAnlyzr;
				dcAnlyzr = new DuplicateConstAnalyzer();
				dcAnlyzr.setMinCloneGroupSize(2);
				dcAnlyzr.setMinLiteralLength(2);
				resp = process(dcAnlyzr, new ExtractConst());
				break;
			case "broad_scope_var":
				BroadScopeVarAnalyzer bvAnlyzr;
				bvAnlyzr = new BroadScopeVarAnalyzer();
				resp = process(bvAnlyzr, new RescopeVar());
				break;
			case "all":
				resp = processAll();
				break;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		resp.put("projectId", projectId);

		// program xml is updated or have not been analyzed yet
		id2XmlSha.put(projectId, sha256hex);
		cache.put(projectId, resp);
		log.info("", resp);
		log.info("save result for {} in cache", projectId);

		return resp;
	}

	public <T extends CodeSmell, R extends RefactRequest<T>> Map<String, Object> process(SmellAnalyzer<T> smellAnalyzer,
			RefactAnalyzer<R> refactAnalyzer) throws Exception {
		Map<String, Object> resp = new HashMap<>();

		Map<String, AnalysisRecord> recordMap = Maps.newHashMap();
		processHelper(smellAnalyzer, refactAnalyzer, recordMap);

		resp.put("records", recordMap);
		return resp;
	}

	public <T extends CodeSmell, R extends RefactRequest<T>> void processHelper(SmellAnalyzer<T> smellAnalyzer,
			RefactAnalyzer<R> refactAnalyzer, Map<String, AnalysisRecord> recordMap) throws Exception {
		SmellListResult<T> smellResult = smellAnalyzer.analyze(program);
		for (int i = 0; i < smellResult.size(); i++) {
			T smell = smellResult.get(i);

			@SuppressWarnings("unchecked")
			R refactReq = (R) smell.buildRefactRequest(program);

			String smellId = smell.getSmellId();
			AnalysisRecord record = new AnalysisRecord(smellId, smell,
					refactAnalyzer.analyze(program.deepCopy(), refactReq));// should analyze a copy not the original
			recordMap.put(smellId, record);
		}
	}

	public Map<String, Object> processAll() {
		Map<String, Object> resp = new HashMap<>();
		Map<String, AnalysisRecord> recordMap = Maps.newHashMap();
		try {

			processHelper(new DuplicateCodeAnalyzer(), new ExtractProcedure(), recordMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			processHelper(new DuplicateSpriteAnalyzer(), new ExtractParentSprite(), recordMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		resp.put("records", recordMap);

		return resp;
	}
}
