package qtutor.adapters;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import anlys.refact.Fragment;
import anlys.smell.DuplicateCode;
import anlys.smell.SmellListResult;
import ast.Stmt;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DuplicateCodeResultListAdapter implements SmellListResultAdapter<DuplicateCode> {

	@ToString
	@Setter
	@Getter
	class DCGroup {
		String id;
		List<String> fragments;
		Map<String, Object> stats = Maps.newHashMap();
	}

	List<DCGroup> dcResultList = Lists.newArrayList();

	public DuplicateCodeResultListAdapter(SmellListResult<DuplicateCode> resultList) {

		for (DuplicateCode cloneGroup : resultList.getInstances()) {
			DCGroup dcGroup = new DCGroup();
			dcGroup.setId(cloneGroup.getSmellId());
			List<String> sb3Frags = Lists.newArrayList();
			List<Fragment> frags = cloneGroup.getFragments();
			for (Fragment f : frags) {
				StringBuilder sb = new StringBuilder();
				StringBuilder debugBlockIds = new StringBuilder();
				for (Stmt stmt : f.getStmts()) {
					try {						
						sb.append(stmt.toSb3());
						debugBlockIds.append(stmt.getId()+", ");
					}catch(Exception e) {
						log.error("@"+stmt.sprite().getName());
						stmt.toSb3();
						e.printStackTrace();
					}
					if(sb.toString().equals("")) {
						System.out.println(debugBlockIds);
					}
//					System.out.println(sb);
					sb.append("\n");
				}
				sb3Frags.add(sb.toString());
			}
			dcGroup.setFragments(sb3Frags);
			dcResultList.add(dcGroup);
			dcGroup.getStats().put("group_size", cloneGroup.getMetadata().get("group_size"));
			dcGroup.getStats().put("fragment_size", cloneGroup.getMetadata().get("instance_size"));
			dcGroup.getStats().put("target", cloneGroup.getTarget());
			
		}
	}

	@Override
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr;
		try {
			jsonStr = mapper.writeValueAsString(dcResultList);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			jsonStr = "{}";
		}
		return jsonStr;
	}

}
