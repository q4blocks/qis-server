package qtutor.adapters;

import anlys.smell.CodeSmell;

public interface SmellListResultAdapter <T extends CodeSmell> {
	public String toJson();
}
