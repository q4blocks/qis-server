package qtutor.adapters;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ExtractCustomBlockParams {
	@JsonProperty
	String target;
	@JsonProperty("fragments")
	List<List<String>> stmtIdFragments;	
}
